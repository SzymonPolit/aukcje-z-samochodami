package com.example.demo.model;

import javax.persistence.*;

//mogłem użyc lomboka wtedy nie musiałbym generować geterów itd

@Entity
//tabelka będzie nazywać się "car"
@Table(name = "car")
public class Car {

    //caly model mojego samochody
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //klucz główny
    private Long id;
    private String brand;
    private String model;
    private int year;
    private int distance;
    private String fuel;
    private String color;
    private int price;
    private String url;

    public Car() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
