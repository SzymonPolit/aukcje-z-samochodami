package com.example.demo.dao;

import com.example.demo.model.Car;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;


import java.util.List;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {

    //Strzeszczenie zapytania do bazy danych.
    String FIND_BY_PROP = "select * from car where car.brand like (?1) and car.model like (?2) and car.year like (?3) and car.fuel like (?4) and car.color like (?5)";

    @Query(value = FIND_BY_PROP, nativeQuery = true)
        //Metoda interfejsu szukająca wszystkich atrybutów
    List<Car> findCarByProperties(String brand, String model, int year, String fuel, String color);

    List<Car> findByBrand(List<String> brand);
}
