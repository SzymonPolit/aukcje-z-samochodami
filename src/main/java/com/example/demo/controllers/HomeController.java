package com.example.demo.controllers;

import com.example.demo.dao.CarRepository;
import com.example.demo.model.Car;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
public class HomeController {

    @Autowired
    CarRepository carRepository;

    public String home() {
        return "index";
    }

    @GetMapping("/add")
    public String add() {
        return "add";
    }

    @PostMapping("/cars")
    public String create(@ModelAttribute Car car) {
        carRepository.save(car);
        return "redirect:/allcars";
    }

    //WYŚWIETLENIE WSZYSTKICH SAMOCHODÓW
    @GetMapping("/allcars")
    public String all(ModelMap modelMap) {
        modelMap.put("posts", carRepository.findAll());
        return "all";
    }

    //WYŚWIETLANIE SAMOCHODU PO ID
    @GetMapping("allcars/{id}")
    public String show(@PathVariable Long id, ModelMap modelMap) {
        modelMap.put("posts", carRepository.findById(id).get());
        return "all";
    }

    @PostMapping("/cars/search")
    public String search(@ModelAttribute("car") Car car, @RequestParam Integer year_from,
                         @RequestParam Integer year_to,
                         ModelMap modelMap) {

        //UTWORZENIE NOWEJ LISTY
        List<String> newList = new ArrayList<>();

        //DODANIE PARAMETRU DO TEJ NOWEJ LISTY
        newList.add(car.getBrand());


        List<Car> cars = carRepository.findByBrand(newList);


        if (cars.isEmpty()) {
            modelMap.put("posts", carRepository.findAll());
            modelMap.put("message", "not found");
        } else {
            modelMap.put("posts", cars);
        }
        return "all";
    }
}