package com.example.demo.controllers;

import com.example.demo.dao.CarRepository;
import com.example.demo.model.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/")
public class Controller {

    @Autowired
    private CarRepository carRepository;

    //dependency injection w konstruktorze ???
    public Controller(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    //metoda api, parametry szukania, czy pole wymagane, domyślna wartość itd.
    @PostMapping("cars")
    public List<Car> findCar(
            @RequestParam(value = "brand", required = false, defaultValue = "") String brand,
            @RequestParam(value = "model", required = false, defaultValue = "") String model,
            @RequestParam(value = "year", required = false, defaultValue = "2014") String year,
            @RequestParam(value = "fuel", required = false, defaultValue = "") String fuel,
            @RequestParam(value = "color", required = false, defaultValue = "") String color) {

        return carRepository.findCarByProperties(brand, model, Integer.valueOf(year), fuel, color);
    }

    //metoda mojego api która dodaje samochód do bazy danych
    @PostMapping("carsadd")
    public void add(@RequestBody Car car) {
        carRepository.save(car);
    }

    //metoda mojego api która wyświetla liste wszystkich samochodów
    @GetMapping("cars/all")
    public List<Car> cars() {
        return carRepository.findAll();

    }
}
